import {
  PoseRenderer,
  PoseOutfitPlugin,
  OutfitParams,
} from "@geenee/bodyrenderers-three";
import { PoseResult } from "@geenee/bodyprocessors";
import * as three from "three";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
import { GLTFLoader } from "three/examples/jsm/loaders/GLTFLoader";

// import { Player /* ... */ } from "@banuba/webar"
// import data from "@banuba/webar/BanubaSDK.data"
// import wasm from "@banuba/webar/BanubaSDK.wasm"
// import simd from "@banuba/webar/BanubaSDK.simd.wasm"
// Renderer


// const player = await Player.create({
//   clientToken: "CbGDgr/kQPL/AUGRtCgZpDRKfEC8pkAjGbiOTzWLmBLc9QyYpAJuYDqQQ+rDVbIt8a9SahzxN70aRjbkRhS+A/AQwa/bZvSZHNBmJuLGiR4vkeduOkiyj2ishAS93++xAVPwZNIR9KcS1HH/GlwNQg6u0g1AdP1Vjsmzl42bsQtyeH0fVn39NhGFdgSNRuFxRfUudqRggIeJLIK3xnDDbBtqaxmpWdW4ERzekWrF7hi4Vb17w4r7Kf1X+sRZ+ya6nDx/5rEdjLOSv+x96Z68/OEop1pDg1h+9vH645gDw0cPODy36bNiaq0NqX69xbEiSlKeQ7HtxHgRxKHPYyWedL4c3IfX+VFceGlct2Tz0DA0Mxoaw3KmmwMCw7AjDwohBswRoxC/x27yoMyUck0bIKdo6+D9DsqtABFtqe8LJJ1o3wEblUnB3emHMh1lQ4XGKgqOuPUXQ9+7b89w3QQ235Ow/xsg58CrxGsSRrljsQ37pUNrFD2URGl8yoISpUNCEDtfAId7/RTnQIWJ6xymQcjkSy9HTCssRpcYTnmWKHnbMePphinPjQCHoQO1z0VFFqBrv2Ga8ijYatHFAXgGlStlLMpld96vay9NaoYOy/eiFVVyh5NtV9xn/SezMEAdaXeDvJcQSnekjuQq5UV/utHwtJJ53F1Af68QhPQNvs4wMWArfloOcUYLLjbZJ2LPYOLOF62+9PFVALIDD8hSfvMlfYSnEcQzkgBmcPlzIkI2/ZmyEpP9ezJbm2F/1FlvUIVlS7UtpWHeR2/qvY3dNHwFj9ZFoiGwprm4HJcbxKZj7cnF32e5tcyFAgOckMMAIIn/coEiKK2sPwC+GJ4oqyGpuGwUtiA6E118A69Wk1hdRAYGaaN4lCmc451jsT1TGEbMMhRVo+xQP7vNowdYRFho9ZJ99Ae3Wk/ADxp92QNSe9QqG7lmDycxjqCoPRifjk5e4Y35uZgoouPMkxQ6RiIV6nMAFMKSoqLaqnUlpNZZy/nWybZNaUXuu9i82ZwkFFlkShGdew8DZz37L7d7JfgixfQwv993+Tgw3oh9/ow7/pa9tTkUfuJ1Rmsae5GCDe8GiKvDinSToeyrcGZGAbMGgl1zwWXNhhxUcUtZ37ZOc2nQsVgJ13VKoAFsMAqjB+t+enRURKab+fiRiEyCelNyT4HNlwtRiAGhyNom3B9IuFND+G0JtM2ak3VJL5qq+dDFYpYLxzYjjpShutCr/YIPW8ud+0uV8tSiFO5/89y3HSNsPQB5ZUlUY+WtgxUrTO4m55Qr0tYdQqw+XRrCCuQP2rPDIqDrNXsdXY+QMlNP5psnCqeWuUPuQiuAwGyDjzTtOvirja+Ybu09gzDSVf9GH+MVWrahdJvsJvGCZiGu8APdH4bQcbbbZYzlgZI6gtgAzClGk6Pwt3fJz9sjbN4kDJSkRLB0NwYdMirGfJWslDHNqLazE+E+GK/bkwE2G45GO8ntKLLlFy+7/pvmBLLmag+zxWvv6awdcDZHmsrZXVB0wSPVBkrS5J5ssUU+S7/lRZGj+YDjccUQ2YHosChfzIz63eNC+sLvDGPPgkw+9xcDgyjUjjtcJKKsdYSP3441gKwr6xMuLnY7Axvbw3Q9he0BrcpwBUe+j94YYv1H0aLIx6K4uPhvkbT5+BTskd6duA8WCVk1/KxVEsMa1gu7WkDPOkTo6QPqsyeZ/G5km5EDOp1hfPp4D1XgiPc4HsOtYY/e3BRNZ/KVK/g2sL789ntgYVePkOCAq127RQM79TzbwAS8NU55ofm1zpOY0YHbv9kzAJorCYge5po2RyYsrmnmGI7ta2YHFh6hPe2fOyNHcHy8J0f+D+djzPVsAkFTF+CTbddEjTPH4/Zv3A02dfoXo9WzRXoowL4u0KF30hvpobweTUFvz9jFH0ay2SsAwjPc3xsYZsoCLafoOn9btJ2LHwO9vrVdlceshnRALSw3LBmyXzlbNRk+9NeubihBWQ/KPc2KxiiFuVvN1HTOHN9wxR+tZwUOyNEb3Mxwrd1jjMmQj4M0Zir4ORaZybtMvAicSPCRcTxm50sn36juAvwQXsDyZCgBN4HR2GKij6uBYuUxRBLa22GqYzZ9HPKbUg1QG1k7CV6F5Q8EK6OfZP8PGEkZnjhoG1Yf2jvZ2RVK5VYCo9BMU7ybdXZo847l0mdHWF/FzfM7YrVWZivLYSSJ5YZHpLJyh5RmBtnIe50gEi/P2kszcPglYNJ7xMU03XxKAUOL1u1lSTIU5bkT7Crn0P4ukWsoCCc9G31tPMF4WIkYbt/9rqRACTFTkNaJDrTQSpXEZCx8W93JeATDmEP/iRDEJJNU3jzKgNrce0PrgfWD06eHbqzKZQ2i9JuckQUZMs68FVI=",
//   // point BanubaSDK where to find these vital files
//   locateFile: {
//     "BanubaSDK.data": data,
//     "BanubaSDK.wasm": wasm,
//     "BanubaSDK.simd.wasm": simd,
//   },
// })

const questList = [
  {
    quest: 'How old is your current mattress?',
    option1: '4 Years or under',
    option2: 'More than 5 years'
  },
  {
    quest: 'Which environmental sleep disruptor are you most concerned with?',
    option1: 'Temperature (sleep hot)',
    option2: 'Noise / loud environment'
  },
  {
    quest: 'What are you more likely to wake up with?',
    option1: 'Pain',
    option2: 'Allergies'
  },
  {
    quest: 'Do you have Sleep apnea or Acid Reflux?',
    option1: 'Yes',
    option2: 'No'
  },
  {
    quest: 'What level of comfort would your perfect mattress be?',
    option1: 'Firmer',
    option2: 'Softer or more plush'
  },
  {
    quest: 'What would make your sleep even better Adjustable base',
    option1: 'Adjustable base',
    option2: 'Pillow & Bedding set'
  },
  {
    quest: '',
    option1: '',
    option2: ''
  },
]
export class AvatarRenderer extends PoseRenderer {
  // Scene
  protected plugin?: PoseOutfitPlugin;
  protected light?: three.PointLight;
  protected ambient?: three.AmbientLight;
  readonly lightInt: number = 0.75;
  readonly ambientInt: number = 1.25;
  // Hands up
  protected handsUp = false;
  protected textModel?: three.Group;
  protected ball?: three.Object3D;
  protected field?: any;
  protected model?: three.Group;
  protected random?: any;
  protected questLevel?: any = 0;
  protected levelTimer?: any = 0;
  protected test?: any = 0;
  // Constructor
  constructor(
    container: HTMLElement,
    mode?: "fit" | "crop",
    mirror?: boolean,
    protected url = "onesie.glb",
    protected outfit?: OutfitParams
  ) {
    super(container, mode, mirror);

  }

  setVideoCanvasStyles() {
    const canvas = document.getElementById("engeenee.canvas.layer0");
    if (canvas) {
      canvas.style.zIndex = "-11";
      canvas.style.borderRadius = "10px";
    }
  }


  // Update
  async update(result: PoseResult, stream: HTMLCanvasElement) {
    const { poses } = result;
    if (poses.length < 1) {
      return super.update(result, stream);
    }
    // Keypoints
    const { points } = poses[0];
   

    let currentLevel = questList[this.questLevel]
    if(currentLevel === undefined){
      const done = document.getElementById('done')
      done.style.display = 'flex'
    }
    const questionHeader = document.getElementById('question-header')
    const option1Header = document.getElementById('option-header-1')
    const option2Header = document.getElementById('option-header-2')

    questionHeader.innerText = currentLevel.quest
    option1Header.innerText = currentLevel.option1
    option2Header.innerText = currentLevel.option2
    console.log(this.levelTimer, this.questLevel)
    if(this.levelTimer === 0 && this.test === 0){
      console.log(this.test, this.levelTimer, this.questLevel)

      this.test = 1
      const that = this
      console.log('boom1')
      setTimeout(function(){
        that.levelTimer = 1
        console.log('boom2')
      }, 3000);
    }
    let that = this;
      if(that.levelTimer === 1){
        if(points.nose.pixel[0] > 0.65){
          console.log('option-1')
          this.test =0;
          that.levelTimer = 0;
          that.questLevel += 1;

        }else if(points.nose.pixel[0] < 0.25){
          console.log('option-2')
          this.test = 0;
          that.levelTimer = 0;
          that.questLevel += 1;
          
        }
      }
    

  
    await super.update(result, stream);
  }
}
