import { FaceRenderer, FaceMaskPlugin } from "@geenee/bodyrenderers-three";
import * as three from "three";
import { RGBELoader } from "three/examples/jsm/loaders/RGBELoader";
import { HalftoneGridMaterial } from "threejs-shader-materials";

export class MaskRenderer extends FaceRenderer {
    // Scene
    protected hat?: three.Object3D;
    protected head?: three.Object3D;
    protected plugin: FaceMaskPlugin;
    protected light?: three.PointLight;
    protected ambient?: three.AmbientLight;
    readonly lightInt = 1;
    readonly ambientInt = 3;

    // Constructor
    constructor(container: HTMLElement, mode?: "fit" | "crop") {
        super(container, mode);
        this.plugin = new FaceMaskPlugin("faceuv.png");
        this.addPlugin(this.plugin);
    }

    // Load assets and setup scene
    async load() {
        if (this.loaded || !this.scene)
            return;
        await this.setupScene(this.scene);
        await super.load();
        // Mask
        const material = new HalftoneGridMaterial();
        material.color = new three.Color(0xff6666);
        material.transparent = true;
        material.speed = 3.0;
        material.isAnimate = true;
        material.division = 24;
        material.waveFrequency = 0.25;
        material.depthTest = false;
        //@ts-ignore
        // this.scene.add(new three.Mesh(this.plugin.mask.geometry, material));
    }

    // Setup scene
    protected async setupScene(scene: three.Scene) {
        // Lightning
        this.light = new three.PointLight(0xFFFFFF, this.lightInt);
        this.ambient = new three.AmbientLight(0xFFFFFF, this.ambientInt);
        this.camera.add(this.light);
        scene.add(this.ambient);
        // Environment
        this.renderer.physicallyCorrectLights = true;
        this.renderer.outputEncoding = three.sRGBEncoding;
        const environment = await new RGBELoader().loadAsync("environment.hdr");
        scene.environment = environment;
    }
}
