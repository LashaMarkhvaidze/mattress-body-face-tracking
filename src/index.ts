import { Engine } from "@geenee/armature";
import { PoseProcessor } from "@geenee/bodyprocessors";
import { OutfitParams } from "@geenee/bodyrenderers-three";
import { AvatarRenderer } from "./avatarrenderer";
import { MaskRenderer } from "./maskrenderer";

import "./index.css";
import { FaceEngine } from "@geenee/bodyprocessors";
import { Snapshoter } from "@geenee/armature";
// Engine
const urlParams = new URLSearchParams(window.location.search);
let rear = urlParams.has("rear");
const engine = new Engine(PoseProcessor, {});
const engine2 = new FaceEngine();
const token = location.hostname === "localhost" ?
    "ND_BlAaxdNVB6c8AlX0cosqHfIMxcoag" : "prod.url_sdk_token";
// Map of available models
const modelMap: { [key: string]: { file: string, avatar: boolean, outfit?: OutfitParams}} = {
    jacket: { file: "jacket.glb", avatar: false},
    sport: { file: "onesie.glb", avatar: true,
    outfit: {
        occluders: [/Head$/, /Body/],
        hidden: [/Eye/, /Teeth/, /Bottom/, /Footwear/, /Glasses/]
    }},
}
let model = "sport";
let avatar = modelMap["sport"].avatar;

// let myAudioElement = new Audio('assets/Tailgate Tour Splash Screen Soundtrack.mp3');


async function main() {
    // Renderer
    // myAudioElement.addEventListener("canplaythrough", event => {
    //     myAudioElement.play();
    //   });
    
    const container = document.getElementById("root");
    if (!container)
        return;
    const renderer2 = new MaskRenderer(container, "crop");

    const renderer = new AvatarRenderer(
        container, "crop", !rear, modelMap[model].file,
        avatar ? undefined : modelMap[model].outfit);
    // Camera switch
    const cameraSwitch = document.getElementById(
        "camera-switch") as HTMLButtonElement | null;
    if (cameraSwitch) {
        cameraSwitch.onclick = async () => {
            cameraSwitch.disabled = true;
            rear = !rear;
            await engine.setup({ size: { width: 1920, height: 1080 }, rear });
            await engine.start();
            renderer.setMirror(!rear);
            cameraSwitch.disabled = true;
        }
    }

    // Initialization
    await Promise.all([
        engine.addRenderer(renderer),
        engine.init(),
        engine2.addRenderer(renderer2),
        engine2.init({ token: token, transform: true, metric: true })
    ])
    await engine.setup({
        size: { width: 1920, height: 1080 }, rear });
    await engine.start();
    await engine2.setup({ size: { width: 1920, height: 1080 }, rear });
    await engine2.start();
    document.getElementById("dots")?.remove();





    
}



main();

